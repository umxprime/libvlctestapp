//
//  SceneDelegate.h
//  libvlcTestApp-iOS
//
//  Created by umxprime on 25/11/2022.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

