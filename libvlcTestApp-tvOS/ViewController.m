//
//  ViewController.m
//  libvlcTestApp-tvOS
//
//  Created by umxprime on 14/11/2022.
//

#import "ViewController.h"
#import "AppDelegate.h"

#include <vlc/vlc.h>

#include <vlc_common.h>
#include <vlc_variables.h>
#include <vlc_plugin.h>

@interface ViewController ()

@end

@implementation ViewController
{
    libvlc_instance_t *_libvlc;
    libvlc_media_player_t *_libvlc_mp;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /* Set VLC_PLUGIN_PATH for dynamic loading */
    NSString *pluginsDirectory = [[NSBundle mainBundle] privateFrameworksPath];
    setenv("VLC_PLUGIN_PATH", [pluginsDirectory UTF8String], 1);

    /* Store startup arguments to forward them to libvlc */
//    NSArray *arguments = [[NSProcessInfo processInfo] arguments];
    NSArray *arguments = @[
//            @"--list-verbose",
            @"-vvvv",
//            @"--video-filter=ci-sharpen", @"--sharpen-sigma=0.1",
            @"--deinterlace-mode=gl_bwdif2x",
            @"--deinterlace-filter=sk_glbwdif",
            @"--sharpen-fast",
//            @"--deinterlace-mode=blend",
//            @"--deinterlace-filter=glblend",
//            @"--deinterlace=0",
//            @"--video-filter=glfastsharpen",
            @"--no-auto-preparse",
            /* If you want to enable the new audio output for AirPlay2 by default */
            // @"--aout=avsamplebuffer",
            @"--no-videotoolbox-error-fallback",
            @"--clock-master=input",
            @"--no-spu",
            @"--no-avstat",
            @"--no-clock-recovery",
            @"--imem-pts-delay=300" /* pts delay in milliseconds */
    ];
    unsigned vlc_argc = (unsigned)[arguments count];
    const char **vlc_argv = malloc(vlc_argc * sizeof *vlc_argv);
    if (vlc_argv == NULL)
        return;
    
    for (unsigned i = 0; i < vlc_argc; i++)
        vlc_argv[i] = [[arguments objectAtIndex:i] UTF8String];
    
    /* Initialize libVLC */
    _libvlc = libvlc_new(vlc_argc, (const char * const*)vlc_argv);
    free(vlc_argv);
    
    if (_libvlc == NULL)
        return;
    
    NSURL *url = [[NSURL alloc] initWithString:@"udp://@:9991"];
    
    libvlc_media_t *m = libvlc_media_new_location([url absoluteString].UTF8String);
    
    libvlc_media_player_t *mp = libvlc_media_player_new(_libvlc);
    libvlc_media_player_set_nsobject(mp, (__bridge void *)self.view);
    libvlc_media_player_set_media(mp, m);
    libvlc_media_player_play(mp);
    libvlc_video_set_sharpen_int(mp, libvlc_sharpen_Enable, 1);
    libvlc_video_set_sharpen_float(mp, libvlc_sharpen_Sigma, 2.0);
    _libvlc_mp = mp;

//    /* Start glue interface, see code below */
//    libvlc_add_intf(_libvlc, "ios_interface,none");
//
//    /* Start parsing arguments and eventual playback */
//    libvlc_playlist_play(_libvlc);
}

/* Glue interface code, define drawable-nsobject for display module */
static int Open(vlc_object_t *obj)
{
    AppDelegate *d = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ViewController *vc = (ViewController *)d.window.rootViewController;
    assert(d != nil && vc != nil);
    var_SetAddress(vlc_object_instance(obj), "drawable-nsobject",
                   (__bridge void *)vc.view);

    return VLC_SUCCESS;
}

#define MODULE_NAME ios_interface
#define MODULE_STRING "ios_interface"
- (IBAction)sliderValueChanged:(id)sender {
}
vlc_module_begin()
    set_capability("interface", 0)
    set_callback(Open)
vlc_module_end()

/* Inject the glue interface as a static module */
typedef int (*vlc_plugin_cb)(vlc_set_cb, void*);

__attribute__((visibility("default")))
vlc_plugin_cb vlc_static_modules[] = { vlc_entry__ios_interface, NULL };

@end
